package main

import (
	"encoding/json"
	"net/http"
)

type Text struct {
	Name    string
	Message []string
}

func main() {
	//port = os.Getenv("PORT")
	http.HandleFunc("/", foo)
	http.ListenAndServe(":3000", nil)
}

func foo(w http.ResponseWriter, r *http.Request) {
	message := Text{"Thando", []string{"Hello", "programming"}}

	jsObject, err := json.Marshal(message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsObject)
}
